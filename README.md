# StockChat
Project requested during Jobisity interview process in March 2022.

# How to run

## docker-compose
Easy!

At the root folder:

```bash
docker-compose build
docker-compose up
```
Then you should be able to see both the service available in the following addresses:<br>
**StockChat.Service**: [https://localhost:5001/swagger]<br>
**StockChat.Web**: [https://localhost:3000]

## local development
To run it locally you will to the following in order:

* Start a local SQL Server instance
    * Run the scripts located on /init-sql-scripts
* Start a local Redis instance
* Compile and run the StockChat.Service application
```bash
dotnet build ./src/StockChat.Service/StockChat.Service.csproj
dotnet run ./src/StockChat.Service/bin/Debug/net6.0/StockChat.Service.dll
```
* Compile and run the StockChat.Bot application
```bash
dotnet build ./src/StockChat.Bot/StockChat.Bot.csproj
dotnet run ./src/StockChat.Bot/bin/Debug/net6.0/StockChat.Bot.dll
```
* Compile and run the StockChat.Web application
```bash
cd ./src/StockChat.Web/
npm i
npm run dev
```

Then you should be able to see both the service available in the following addresses:<br>
**StockChat.Service**: [https://localhost:7212/swagger]<br>
**StockChat.Web**: [https://localhost:3000]

# Solution Architecture

The following diagram represents the solution architecture:

[![alt](docs/solution-architecture.png)]

# Tech Stack

The following technologies were chosen for each project:

## StockChat.Bot

* .NET Core 6

### Main Libraries

* SignalR
* StackExchange.Redis
* Serilog

## StockChat.Service

* ASP.NET Core 6

### Main Libraries

* SignalR
* SignalR Redis
* AutoMapper
* Swashbuckle
* Serilog

## StockChat.Core

* .NET Core 6

### Main Libraries

* FluentValidation

## StockChat.Repository.SqlServer

* .NET Core 6

### Main Libraries

* Dapper
* Dapper.Apex (My own library at: [Github](https://github.com/renanmt/Dapper.Apex))

## StockChat.Web

* Node.Js
* Typescript

### Main Libraries

* Svelte

# Information and Considerations

During the development I took an architecture design for the solution based on how I normally build my own services.<br>
The solution has the Service controllers as their main entry point and the requests are passed to the Core library classes that are defined in a mix of `n-tier` and `clean architecture` designs.<br>

It looks like this:

<img src="docs/service-architecure.png" width="600">


`Workflow `clases are the entry point of the `Core library` and serve as the orchastrators of the every operation in the solution. They apply the Facade design pattern to hide the complexity of busines requirements behind a simple flow.
The workflow will make calls to the business layer in the correct order to perform each operation.<br>
The `Business` layer contain the business rules defined for each entity in the application, including validation. This layer also communicates with the repository layer for data writing and fetching.<br>
The `Repository` layer is represented in the `Core library` by a set of interfaces, decoupling it from its actual implementation, allowing it for easier database changes and migrations in the future.

Some things may look overkill but I prefer to always stay prepared for the future.

## Authentication

I know the document requested the implementation of `Identity` for authentication, but I decided to go with JWT. Not only because I believe JWT authentication when correctly implemented is better, but also because `Identity` somewhat forces you to use `Entity Framework` (no wonder, it's made by Microsoft).

## Dapper / Dapper.Apex vs Entity Framework

I am not a big fan of `EF` because it creates a way too complex layer (and most of times unecessary) in front of the Database. It can be tricky to deal with query execution plans. For speed and scalability reasons I prefer a simple and fast solution like Dapper.<br>
This choice also gave me the opportunity to show my own library `Dapper.Apex`, that replaces `Dapper.Contrib` from StackExchange. As a contributor to Dapper, I ran tired of waiting for my merge requests to be accepted so I decided to build my own, and **BETTER**.

Visit: [Github](https://github.com/renanmt/Dapper.Apex)

## Exception Handling

You are probably wondering: 

*- Where is the exception handling in this service?*

Well, all (mostly) exceptions thrown by the `Core` are a set of special exception classes that contain information about the problem, http status code and a fault code to help the client identify different cases, like when they receive a HTTP 400.<br>
On the service side, a single middleware is responsible to catch all expected and unexpected thrown exceptions, treat and format them in a standard exception message that all clients ca n read.

## Redis with SignalR

I decided to use Redis not only for the queue with bot but also to use together with SignalR, allowing the service to scale.

## Things I wish I had time for

* Unit and Integration Tests (´。＿。｀)
* A better health check for the service
* A circuit breaker implementation for service dependencies
* A CI/CD pipeline for cloud deployment

# Questions? Issues?

Feel free to drop an issue here in Gitlab and I will be happy to check.