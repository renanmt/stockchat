﻿CREATE TABLE [sc].[User]
(
    [Id] UNIQUEIDENTIFIER NOT NUlL,
    [Email] VARCHAR(100) NOT NULL,
    [Username] VARCHAR(20) NOT NULL,
    [PasswordHash] CHAR(72) NOT NULL,
    CONSTRAINT PK_User PRIMARY KEY ([Id])
)
GO

CREATE UNIQUE INDEX UX_User_Email ON [sc].[User] ([Email])
GO

CREATE UNIQUE INDEX UX_User_Username ON [sc].[User] ([Username])
GO