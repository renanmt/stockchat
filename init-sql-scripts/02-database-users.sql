CREATE
LOGIN [StockChatService] WITH PASSWORD = 'EP6EjGoT4h8zJeBa'
GO

CREATE
USER [StockChatService]
	FOR LOGIN [StockChatService]
GO

GRANT CONNECT TO [StockChatService]
GO

GRANT
SELECT,
UPDATE,
INSERT
,
DELETE
ON SCHEMA :: sc
    TO [StockChatService]
    GO