using System;
using System.Threading.Tasks;
using AutoFixture;
using FluentValidation;
using Moq;
using StockChat.Core.Business;
using StockChat.Core.Exceptions;
using StockChat.Core.Models;
using StockChat.Core.Repository;
using StockChat.Entities;
using Xunit;

namespace StockChat.Tests.Unit;

public class UserBusinessTest
{
    [Fact]
    public async Task CreateUser_MissingNewUserParameter()
    {
        var userRepoMock = new Mock<IUserRepository>();
        var userValidatorMock = new Mock<IValidator<NewUser>>();

        IUserBusiness userBusiness = new UserBusiness(userRepoMock.Object, userValidatorMock.Object);

        var createUser = () => userBusiness.CreateUser(null, null);

        var ex = await Assert.ThrowsAsync<ArgumentNullException>(createUser);
        Assert.Equal("newUser", ex.ParamName);
    }

    [Fact]
    public async Task CreateUser_MissingPasswordHashParameter()
    {
        var userRepoMock = new Mock<IUserRepository>();
        var userValidatorMock = new Mock<IValidator<NewUser>>();
        var fixture = new Fixture();
        var newUser = fixture.Create<NewUser>();

        IUserBusiness userBusiness = new UserBusiness(userRepoMock.Object, userValidatorMock.Object);

        var createUser = () => userBusiness.CreateUser(newUser, null);

        var ex = await Assert.ThrowsAsync<ArgumentNullException>(createUser);
        Assert.Equal("passwordHash", ex.ParamName);
    }

    [Fact]
    public async Task CreateUser_ValidFlow()
    {
        var userRepoMock = new Mock<IUserRepository>();
        userRepoMock.Setup(repo => repo.CreateAsync(It.IsAny<User>()))
            .Returns((User user) =>
            {
                user.Id = Guid.NewGuid();
                return Task.FromResult(user);
            });

        var userValidatorMock = new Mock<IValidator<NewUser>>();
        var fixture = new Fixture();
        var newUser = fixture.Create<NewUser>();
        var passwordHash = fixture.Create<string>();

        IUserBusiness userBusiness = new UserBusiness(userRepoMock.Object, userValidatorMock.Object);

        var user = await userBusiness.CreateUser(newUser, passwordHash);

        Assert.NotNull(user);
        Assert.NotEqual(Guid.Empty, user.Id);
        Assert.Equal(newUser.Email, user.Email);
        Assert.Equal(newUser.Username, user.Username);
        Assert.Equal(passwordHash, user.PasswordHash);
    }

    [Fact]
    public async Task GetUser_NotFound()
    {
        var userRepoMock = new Mock<IUserRepository>();
        userRepoMock.Setup(repo => repo.GetAsync(It.IsAny<Guid>()))
            .Returns((Guid id) => Task.FromResult((User?) null));
        
        var userValidatorMock = new Mock<IValidator<NewUser>>();

        IUserBusiness userBusiness = new UserBusiness(userRepoMock.Object, userValidatorMock.Object);

        var getUser = () => userBusiness.GetUser(Guid.NewGuid());

        var ex = Assert.ThrowsAsync<ResourceNotFoundException>(getUser);
    }
    
    [Fact]
    public async Task GetUser_ValidFlow()
    {
        var fixture = new Fixture();
        var user = fixture.Create<User>();
        
        var userRepoMock = new Mock<IUserRepository>();
        userRepoMock.Setup(repo => repo.GetAsync(It.IsAny<Guid>()))
            .Returns((Guid id) => Task.FromResult(user)!);
        
        var userValidatorMock = new Mock<IValidator<NewUser>>();

        IUserBusiness userBusiness = new UserBusiness(userRepoMock.Object, userValidatorMock.Object);

        var result = await userBusiness.GetUser(Guid.NewGuid());

        Assert.NotNull(result);
        Assert.Same(user, result);
    }

    //
    // If you hit this point of the code, you are probably wondering, where are the rest of the tests.
    // I'm sorry, but with my currently available time divided between family and work, this is all I was able to pull.
    //
    // If you want to see some of my work with Tests, you can check it here: https://github.com/renanmt/Dapper.Apex/tree/master/Dapper.Apex.Test
    // Those are tests written for my ORM library built on top of Dapper that I'm also using in this project.
    //
    // But, don't leave empty handed. here's a cookie:
    //
    //     ___
    //   /^   \
    //  |^   ^|
    //  \____/
    // 
    //
}