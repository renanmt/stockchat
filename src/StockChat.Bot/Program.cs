using Serilog;
using StackExchange.Redis;
using StockChat.Bot;
using StockChat.Bot.Strategy;

var host = Host.CreateDefaultBuilder(args)
    .UseSerilog((context, configuration) =>
        configuration
            .MinimumLevel.Information()
            .WriteTo.Console()
    )
    .ConfigureAppConfiguration((context, builder) =>
    {
        var environmentName = Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT");

        builder.AddEnvironmentVariables()
            .AddJsonFile("appsettings.json", true, true)
            .AddJsonFile($"appsettings.{environmentName}.json", true, true)
            .Build();
    })
    .ConfigureServices((builder, services) =>
    {
        services.AddLogging();
        services.AddHostedService<Worker>();
        services.AddHttpClient();
        
        var redisHost = builder.Configuration.GetSection("Redis").Value;
        var hubHost = builder.Configuration.GetSection("Hub").Value;

        services.AddSingleton<IConnectionMultiplexer>(ConnectionMultiplexer.Connect(redisHost));
        services.AddSingleton(new HubMessenger(hubHost));

        //
        // This is overkill, but will support future bot needs
        //
        services.AddSingleton<IBotStrategy, StockMessageProcessor>();
        services.AddSingleton<IList<IBotStrategy>>(provider => provider.GetServices<IBotStrategy>().ToList());
        services.AddSingleton<BotProcessorResolver>();

    })
    .Build();

await host.RunAsync();