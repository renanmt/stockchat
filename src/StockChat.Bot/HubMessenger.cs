using System.Text.Json;
using Microsoft.AspNetCore.SignalR.Client;
using StockChat.Entities;

namespace StockChat.Bot;

public class HubMessenger
{
    private readonly HubConnection connection;

    public HubMessenger(string host)
    {
        connection = new HubConnectionBuilder()
            .WithUrl(host)
            .Build();

        connection.Closed += async error =>
        {
            await Task.Delay(new Random().Next(0, 5) * 1000);
            await connection.StartAsync();
        };
    }

    public async Task SendResponse(BotResponse botResponse)
    {
        if (connection.State == HubConnectionState.Disconnected) await connection.StartAsync();
        var botReponseJson = JsonSerializer.Serialize(botResponse);
        await connection.InvokeAsync("BotResponse", botReponseJson);
    }
}