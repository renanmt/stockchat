using System.Text.Json;
using StackExchange.Redis;
using StockChat.Bot.Strategy;
using StockChat.Entities;

namespace StockChat.Bot;

public class Worker : BackgroundService
{
    private readonly BotProcessorResolver _botProcessorResolver;
    private readonly HubMessenger _hubMessenger;
    private readonly ILogger<Worker> _logger;
    private readonly IConnectionMultiplexer _redis;

    public Worker(ILogger<Worker> logger, IConnectionMultiplexer redis, BotProcessorResolver botProcessorResolver,
        HubMessenger hubMessenger)
    {
        _logger = logger;
        _redis = redis;
        _botProcessorResolver = botProcessorResolver;
        _hubMessenger = hubMessenger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var subscriber = _redis.GetSubscriber();

        //
        // Redis Queue processing happens here
        //
        (await subscriber.SubscribeAsync("BotCommand"))
            .OnMessage(async receivedMessage =>
            {
                var message = JsonSerializer.Deserialize<BotMessage>(receivedMessage.Message);
                var messageProcessor = _botProcessorResolver.GetBotForMessage(message!);

                if (messageProcessor != null)
                {
                    var response = await messageProcessor.Process(message!);
                    if (response != null)
                    {
                        _logger.LogInformation("{botName} processed message: {message}", messageProcessor.BotName, response.Message);
                        response.RoomId = message!.RoomId;
                        await _hubMessenger.SendResponse(response);
                    }
                }
            });

        while (!stoppingToken.IsCancellationRequested)
        {
            _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
            await Task.Delay(5000, stoppingToken);
        }
    }
}