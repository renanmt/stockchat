using StockChat.Entities;

namespace StockChat.Bot.Strategy;

public class BotProcessorResolver
{
    private readonly IList<IBotStrategy> _botStrategies;

    public BotProcessorResolver(IList<IBotStrategy> botStrategies)
    {
        _botStrategies = botStrategies;
    }

    public IBotStrategy? GetBotForMessage(BotMessage botMessage)
    {
        return _botStrategies.FirstOrDefault(b => b.CanHandle(botMessage));
    }
}