using StockChat.Entities;

namespace StockChat.Bot.Strategy;

public interface IBotStrategy
{
    Task<BotResponse?> Process(BotMessage botMessage);
    bool CanHandle(BotMessage botMessage);
    
    string BotName { get; }
}