using System.Text.Json;
using System.Text.RegularExpressions;
using StockChat.Entities;

namespace StockChat.Bot.Strategy;

public class StockMessageProcessor : IBotStrategy
{
    private readonly IHttpClientFactory _httpClientFactory;
    private static readonly Regex _stockRegex = new(@"^\/stock=([\w|\.]+)$");
    private static readonly string _stockHost = "https://stooq.com/q/l/?s={0}&f=sd2t2ohlcv&h&e=json";
    private static readonly string _stockReponse = "{0} quote is ${1} per share";

    public StockMessageProcessor(IHttpClientFactory httpClientFactory)
    {
        _httpClientFactory = httpClientFactory;
    }

    public async Task<BotResponse?> Process(BotMessage botMessage)
    {
        var stock = _stockRegex.Match(botMessage.Body).Groups[1].Value;
        BotResponse? botResponse = null;
        try
        {
            var httpClient = _httpClientFactory.CreateClient();
            var response = await httpClient.GetAsync(string.Format(_stockHost, stock));
            var json = await response.Content.ReadAsStringAsync();

            var stooqResponse = JsonSerializer.Deserialize<StooqResponse>(json,
                new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });

            var stockInfo = stooqResponse!.Symbols.FirstOrDefault(s =>
                s.Symbol!.Equals(stock, StringComparison.InvariantCultureIgnoreCase));

            if (stockInfo != null)
                botResponse = new BotResponse
                {
                    BotName = BotName,
                    Message = string.Format(_stockReponse, stockInfo.Symbol!.ToUpper(), stockInfo.Close)
                };
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return botResponse;
    }

    public bool CanHandle(BotMessage botMessage)
    {
        return _stockRegex.IsMatch(botMessage.Body);
    }

    public string BotName => "StockBot";
}

internal class StooqResponse
{
    public IList<StockInfo> Symbols { get; set; }
}

internal class StockInfo
{
    public string? Symbol { get; set; }
    public string? Date { get; set; }
    public string? Time { get; set; }
    public decimal Open { get; set; }
    public decimal High { get; set; }
    public decimal Low { get; set; }
    public decimal Close { get; set; }
    public uint Volume { get; set; }
}