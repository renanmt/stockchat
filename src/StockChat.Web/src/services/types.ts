export type ErrorResponse = {
    message: string;
    faultCode: number;
    statusCode: number;
    stackTrace: string | null;
    exceptionTime: Date;
    traceIdentifier: string | null;
    additionalInfo: Array<Failure> | null;
    originalException: object | null;
};

export type Failure = {
    errorMessage: string;
    propertyName: string;
    attemptedValue: any;
};
