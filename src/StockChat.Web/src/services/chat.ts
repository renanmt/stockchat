import {
    HubConnection,
    HubConnectionBuilder,
    LogLevel
} from "@microsoft/signalr";

import { readable } from "svelte/store";
import type { Subscriber } from "svelte/store";
import moment from "moment";
import axios from "axios";

import { httpHost } from ".";
import type { Room } from "../types";
import { messages, room, roomUsers } from "../stores";

let connection: HubConnection;
let setConnected: Subscriber<boolean>;

export const connected = readable(false, function start(set) {
    setConnected = set;
});

export async function getRooms(token: string): Promise<Array<Room>> {
    try {
        const rooms: Array<Room> = (
            await axios.get(`${httpHost}/api/rooms`, {
                headers: { Authorization: `bearer ${token}` }
            })
        ).data;

        return rooms;
    } catch (e) {
        console.error(e);
    }
}

export async function joinRoom(token: string, room: string): Promise<void> {
    try {
        const conn = new HubConnectionBuilder()
            .withUrl(`${httpHost}/chat`, {
                accessTokenFactory: () => token
            })
            .withAutomaticReconnect()
            .configureLogging(LogLevel.Information)
            .build();

        conn.on("ConnectedUsers", (users: Array<string>) => {
            roomUsers.set(users);
        });

        conn.on("ReceiveMessage", (username, createdAt, message) => {
            messages.push({
                author: username,
                body: message,
                createdAt,
                createdAtNice: moment(createdAt).calendar()
            });
        });

        await conn.start();
        await conn.invoke("JoinRoom", room);

        connection = conn;
        setConnected(true);
    } catch (error) {
        console.error(error);
    }
}

export async function sendMessage(message: string) {
    try {
        await connection.invoke("SendMessage", message);
    } catch (error) {
        console.error(error);
    }
}

export async function leaveRoom() {
    try {
        await connection.invoke("LeaveRoom");
        room.set(null);
        messages.clear();
        await connection.stop();
        setConnected(false);
    } catch (error) {
        console.error(error);
    }
}
