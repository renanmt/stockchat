import axios from "axios";

import { readable } from "svelte/store";
import type { Readable, Subscriber } from "svelte/store";
import type { ErrorResponse } from "./types";
import { setToken } from "../stores";
import { httpHost } from ".";
import type { Token } from "../types";

let setLoginError: Subscriber<ErrorResponse>;
let setRegisterError: Subscriber<ErrorResponse>;

export const loginError: Readable<ErrorResponse> = readable(
    null,
    function start(set) {
        setLoginError = set;
    }
);

export const registerError: Readable<ErrorResponse> = readable(
    null,
    function start(set) {
        setRegisterError = set;
    }
);

export type NewUser = {
    email: string;
    username: string;
    password: string;
};

type UserRequest = {
    username: string;
    password: string;
};

export async function login(username: string, password: string) {
    const request: UserRequest = {
        username,
        password
    };

    try {
        const response: Token = (
            await axios.post(`${httpHost}/api/sessions`, request)
        ).data;
        setToken(response);
    } catch (e) {
        setLoginError(e.response.data);
    }
}

export async function register(newUser: NewUser): Promise<Boolean> {
    try {
        await axios.post(`${httpHost}/api/users`, newUser);
        return true;
    } catch (e) {
        setRegisterError(e.response.data);
        return false;
    }
}
