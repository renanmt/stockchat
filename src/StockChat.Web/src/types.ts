export type Person = {
    id: string;
    name: string;
    avatar: number;
};

export type Token = {
    username: string;
    token: string;
};

export type Room = {
    id: string;
    name: string;
};

export type Message = {
    body: string;
    author: string;
    createdAt: Date;
    createdAtNice: string;
};
