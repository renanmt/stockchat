import {
    readable,
    writable,
    type Readable,
    type Subscriber,
    type Writable
} from "svelte/store";
import type { Message, Token } from "../types";

export let setToken: Subscriber<Token>;

export const userToken: Readable<Token> = readable(null, function start(set) {
    setToken = set;
});

export const room: Writable<string> = writable(null);

function createMessages() {
    const { subscribe, set, update } = writable([]);

    return {
        subscribe,
        push: (message: Message) =>
            update((msgs) => {
                if (msgs.length === 50) msgs.shift();
                msgs.push(message);
                return msgs;
            }),
        clear: () => {
            update((msgs) => []);
        }
    };
}

export const messages = createMessages();

export const roomUsers: Writable<Array<string>> = writable([]);
