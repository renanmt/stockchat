const emailRegex =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export function validateEmail(email: string): Boolean {
    return emailRegex.test(email.toLowerCase());
}

export function getAvatar(n: number): string {
    if (n === 0) return "fat_bot.png";
    return `https://bootdey.com/img/Content/avatar/avatar${n}.png`;
}
