﻿using System.Text.RegularExpressions;
using FluentValidation;
using StockChat.Core.Exceptions;
using StockChat.Core.Models;
using StockChat.Core.Repository;
using StockChat.Entities;

namespace StockChat.Core.Business;

public class UserBusiness : BaseEntityBusiness, IUserBusiness
{
    private readonly IValidator<NewUser> _newUserValidator;
    private readonly IUserRepository _userRepository;

    private readonly Regex _emailRegex =
        new Regex(
            @"^(([^<>()[\]\\.,;:\s@""]+(\.[^<>()[\]\\.,;:\s@""]+)*)|("".+""))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$"); 

    public UserBusiness(IUserRepository userRepository, IValidator<NewUser> newUserValidator)
    {
        _userRepository = userRepository;
        _newUserValidator = newUserValidator;
    }

    public async Task<User> CreateUser(NewUser newUser, string passwordHash)
    {
        ArgumentNullException.ThrowIfNull(newUser);
        ArgumentNullException.ThrowIfNull(passwordHash);

        var user = new User
        {
            Email = newUser.Email,
            Username = newUser.Username,
            PasswordHash = passwordHash
        };

        await _userRepository.CreateAsync(user);

        return user;
    }

    public async Task<User> GetUser(Guid id)
    {
        var user = await _userRepository.GetAsync(id);
        
        ResourceNotFoundException.ThrowIfNull(user);

        return user!;
    }

    public async Task<User> GetUser(string username)
    {
        if (string.IsNullOrEmpty(username))
            throw new MissingParameterException(nameof(username));

        User? user;
        if (_emailRegex.IsMatch(username))
            user = await _userRepository.GetByEmailAsync(username);
        else
            user = await _userRepository.GetByUsernameAsync(username);
        
        ResourceNotFoundException.ThrowIfNull(user);

        return user!;
    }

    public void ValidateNewUser(NewUser newUser)
    {
        base.ValidateAndThrow(newUser, _newUserValidator);
    }
}