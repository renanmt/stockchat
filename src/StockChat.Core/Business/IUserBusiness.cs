﻿using StockChat.Core.Models;
using StockChat.Entities;

namespace StockChat.Core.Business;

public interface IUserBusiness
{
    Task<User> CreateUser(NewUser newUser, string passwordHash);
    Task<User> GetUser(Guid id);
    Task<User> GetUser(string username);
    void ValidateNewUser(NewUser newUser);
}