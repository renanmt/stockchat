using FluentValidation;
using FluentValidation.Results;
using StockChat.Core.Business.Validators;
using StockChat.Core.Exceptions;

namespace StockChat.Core.Business;

public abstract class BaseEntityBusiness
{
    public virtual void ValidateAndThrow<T>(T model, IValidator<T>? validator)
    {
        if (model == null)
            throw new MissingParameterException($"{typeof(T)} object cannot be null");

        if (validator == null) return;
        
        var validationResult = validator.Validate(model);

        if (validationResult != null && !validationResult.IsValid)
            throw new ModelValidationException(validationResult.Errors);
    }

    protected IEnumerable<Failure> GetFailures(IList<ValidationFailure> validationFailures)
    {
        if (validationFailures == null)
            throw new ArgumentNullException(nameof(validationFailures));

        foreach (var failure in validationFailures)
            yield return new Failure
            {
                ErrorMessage = failure.ErrorMessage,
                PropertyName = failure.PropertyName,
                AttemptedValue = failure.AttemptedValue
            };
    }
}