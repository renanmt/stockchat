namespace StockChat.Core.Business.Validators;

public class Failure
{
    public string ErrorMessage { get; set; } = null!;
    public string PropertyName { get; set; } = null!;
    public object AttemptedValue { get; set; } = null!;
}