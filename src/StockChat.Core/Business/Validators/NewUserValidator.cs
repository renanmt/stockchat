using FluentValidation;
using StockChat.Core.Models;

namespace StockChat.Core.Business.Validators;

public class NewUserValidator : AbstractValidator<NewUser>
{
    public NewUserValidator()
    {
        RuleFor(m => m.Email).EmailAddress().NotEmpty();

        RuleFor(m => m.Username).NotEmpty();

        RuleFor(m => m.Password).Matches(@"[a-z]+")
            .WithMessage("Password should contain at least one lower case letter");
        RuleFor(m => m.Password).Matches(@"[A-Z]+")
            .WithMessage("Password should contain at least one upper case letter");
        RuleFor(m => m.Password).Matches(@"[0-9]+")
            .WithMessage("Password should contain at least one numeric value");
        RuleFor(m => m.Password).Matches(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]")
            .WithMessage("Password should contain at least one special character");
        RuleFor(m => m.Password).Matches(@".{4,}")
            .WithMessage("Password should contain at least 4 characters");
    }
}