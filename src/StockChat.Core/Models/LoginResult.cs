using StockChat.Entities;

namespace StockChat.Core.Models;

public class LoginResult
{
    public Guid Id { get; set; }
    public string Username { get; set; } = null!;
    public string Email { get; set; } = null!;
    public string Token { get; set; } = null!;
}