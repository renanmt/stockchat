﻿using System.Net;

namespace StockChat.Core.Exceptions;

public class ForbiddenException : ApiException
{
    public ForbiddenException(string message, Exception? innerException = null)
        : base(
            message,
            HttpStatusCode.Forbidden,
            FaultCode.InvalidPermission,
            innerException)
    {
    }
}