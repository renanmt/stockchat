﻿using System.Net;

namespace StockChat.Core.Exceptions;

public class ApiException : Exception
{
    public ApiException(string message, HttpStatusCode statusCode = HttpStatusCode.InternalServerError,
        FaultCode faultCode = FaultCode.InternalServerError, Exception? innerException = null) : base(message,
        innerException)
    {
        StatusCode = statusCode;
        FaultCode = faultCode;
        ExceptionTime = DateTime.UtcNow;
    }

    public HttpStatusCode StatusCode { get; protected set; }
    public FaultCode FaultCode { get; }
    public DateTime ExceptionTime { get; protected set; }
}