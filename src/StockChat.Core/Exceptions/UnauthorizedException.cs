﻿using System.Net;

namespace StockChat.Core.Exceptions;

public class UnauthorizedException : ApiException
{
    public UnauthorizedException(string message, Exception? innerException = null)
        : base(
            message,
            HttpStatusCode.Unauthorized,
            FaultCode.Unauthorized,
            innerException)
    {
    }
}