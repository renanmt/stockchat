﻿using System.Net;

namespace StockChat.Core.Exceptions;

public class InvalidParameterException : ApiException
{
    public InvalidParameterException(string message, FaultCode faultCode = FaultCode.InvalidParameter,
        Exception? innerException = null)
        : base(
            message,
            HttpStatusCode.BadRequest,
            faultCode,
            innerException)
    {
    }
}