﻿namespace StockChat.Core.Exceptions;

public enum FaultCode
{
    //
    // Default Codes
    //

    // HTTP 400 Bad Request
    InvalidParameter = 40001, // An expected Parameter in the URL is invalid, wrong format/type etc
    MissingParameter = 40002, // An expected Parameter in the URL is missing
    InvalidJsonFormat = 40003, // Invalid JSON object has been passed into the service
    OutOfRangeParameter = 40004, // An expected Parameter in the URL is out of the permitted range
    LengthOfJsonTooBig = 40005, // JSON length passed in has a maximum limit

    // HTTP 401 Unauthorized
    Unauthorized = 40101, // The ticket type is wrong, i.e. player ticket, anonymous etc
    ExpiredTicket = 40102, // The ticket has expired, need to refresh the ticket i.e. login again
    OriginNotAllowed = 40103, // The origin of the request is not allowed

    // HTTP 403 Forbidden
    Forbidden = 40300, // Current user can not change the resource, as they are not the owner
    InvalidPermission = 40301, // The user does not have the required permissions

    // HTTP 404 Not Found
    ResourceNotFound = 40400, // The Requested resource was not found

    // HTTP 405 Method Not Allowed
    MethodNotAllowed = 40500, // HTTP method not allowed on the resource

    // HTTP 409 Data Conflict
    DataPersistenceError = 40901, // Error at DB layer.
    AlreadyExists = 40902, // The value already exists fault

    OldVersionOfResource =
        40903, // Client is attempting to change a resource that has already been updated since it took a copy, therefore this will overwrite the latest changes

    // HTTP 410 Gone
    Gone = 41000, // Resource deleted

    // HTTP 415 Unsupported Media Type
    InvalidContentType = 41500, // The content type is invalid

    // HTTP 500 HAL Server Failure
    InternalServerError =
        50000, // The HAL 9000 series is the most reliable computer ever made. Un-handled exception caught.


    //
    // Custom codes
    //

    // HTTP 400 Bad Request
    PasswordNotSecure = 4000101
}