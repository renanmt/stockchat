﻿using System.Net;

namespace StockChat.Core.Exceptions;

public class MissingParameterException : ApiException
{
    public MissingParameterException(string message, Exception? innerException = null)
        : base(
            message,
            HttpStatusCode.BadRequest,
            FaultCode.MissingParameter,
            innerException)
    {
    }
}