using FluentValidation.Results;
using StockChat.Core.Business.Validators;

namespace StockChat.Core.Exceptions;

public class ModelValidationException : InvalidParameterException
{
    public ModelValidationException(IEnumerable<ValidationFailure>? failures)
        : base("Validation Exception")
    {
        if (failures == null) return;

        var parsedFailures = GetFailures(failures);
        Data.Add("additionalInfo", parsedFailures);
    }

    public ModelValidationException(IEnumerable<Failure> failures)
        : base("Validation Exception")
    {
        if (failures != null)
            Data.Add("additionalInfo", failures);
    }

    public IEnumerable<Failure> GetFailures(IEnumerable<ValidationFailure> failures)
    {
        return failures.Select(failure => new Failure
        {
            ErrorMessage = failure.ErrorMessage,
            PropertyName = failure.PropertyName,
            AttemptedValue = failure.AttemptedValue
        });
    }
}