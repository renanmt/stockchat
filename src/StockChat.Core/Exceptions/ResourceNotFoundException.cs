﻿using System.Net;

namespace StockChat.Core.Exceptions;

public class ResourceNotFoundException : ApiException
{
    public ResourceNotFoundException(string message, FaultCode faultCode = FaultCode.ResourceNotFound,
        Exception? innerException = null)
        : base(
            message,
            HttpStatusCode.NotFound,
            faultCode,
            innerException)
    {
    }

    public static void ThrowIfNull<T>(T value)
    {
        if (value == null)
            throw new ResourceNotFoundException($"{typeof(T).Name} not found");
    }
}