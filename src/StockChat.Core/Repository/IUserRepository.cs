﻿using StockChat.Entities;

namespace StockChat.Core.Repository;

public interface IUserRepository
{
    Task CreateAsync(User user);
    Task<User?> GetAsync(Guid id);
    Task<User?> GetByUsernameAsync(string username);
    Task<User?> GetByEmailAsync(string email);
}