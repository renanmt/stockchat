﻿using StockChat.Core.Models;
using StockChat.Entities;

namespace StockChat.Core.Workflow;

public interface IUserWorkflow
{
    Task<LoginResult> Login(UserCredentials credentials);
    Task<User> Register(NewUser newUser);
    Task<User> GetUser(Guid id);
}