﻿using StockChat.Core.Business;
using StockChat.Core.Exceptions;
using StockChat.Core.Models;
using StockChat.Entities;
using StockChat.Security;

namespace StockChat.Core.Workflow;

public class UserWorkflow : IUserWorkflow
{
    private readonly IPasswordStorage _passwordStorage;
    private readonly ITokenService _tokenService;
    private readonly IUserBusiness _userBusiness;

    public UserWorkflow(IUserBusiness userBusiness, IPasswordStorage passwordStorage, ITokenService tokenService)
    {
        _userBusiness = userBusiness;
        _passwordStorage = passwordStorage;
        _tokenService = tokenService;
    }

    public async Task<User> Register(NewUser newUser)
    {
        _userBusiness.ValidateNewUser(newUser);

        var passwordHash = _passwordStorage.CreateHash(newUser.Password);

        var user = await _userBusiness.CreateUser(newUser, passwordHash);

        return user;
    }

    public Task<User> GetUser(Guid id)
    {
        var user = _userBusiness.GetUser(id);
        
        return user;
    }

    public async Task<LoginResult> Login(UserCredentials credentials)
    {
        var user = await _userBusiness.GetUser(credentials.Username);

        var success = _passwordStorage.VerifyPassword(credentials.Password, user.PasswordHash);

        if (!success) throw new UnauthorizedException("Invalid credentials");

        var token = _tokenService.GenerateToken(user);

        var result = new LoginResult()
        {
            Id = user.Id,
            Email = user.Email,
            Username = user.Username,
            Token = token
        };

        return result;
    }
}