﻿namespace StockChat.Entities;

public class BotMessage
{
    public Guid RoomId { get; set; }
    public string Username { get; set; } = null!;
    public string Body { get; set; } = null!;
    public DateTime CreatedAt { get; set; }
}