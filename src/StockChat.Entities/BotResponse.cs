namespace StockChat.Entities;

public class BotResponse
{
    public Guid RoomId { get; set; }
    public string BotName { get; set; } = null!;
    public string Message { get; set; } = null!;
}