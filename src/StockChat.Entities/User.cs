﻿using Dapper.Apex;

namespace StockChat.Entities;

[Table("[sc].[User]")]
public class User
{
    [ExplicitKey]
    public Guid Id { get; set; }
    public string Email { get; set; } = null!;
    public string Username { get; set; } = null!;
    public string PasswordHash { get; set; } = null!;
}