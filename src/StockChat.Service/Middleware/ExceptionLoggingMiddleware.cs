﻿using StockChat.Service.Models;

namespace StockChat.Service.Middleware;

public class ExceptionLoggingMiddleware : ExceptionHandlerMiddleware
{
    private readonly ILogger _logger;

    public ExceptionLoggingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory, IWebHostEnvironment env) :
        base(next, env)
    {
        _logger = loggerFactory.CreateLogger<ExceptionLoggingMiddleware>();
    }

    protected override void ProcessError(ErrorResponse response, Exception exception)
    {
        base.ProcessError(response, exception);

        var innerResponse = exception.InnerException != null ? GetResponse(exception.InnerException) : null;

        _logger.LogError(exception, $"Status Code: {response.StatusCode} | {response.Message}");

        if (innerResponse != null)
        {
            _logger.LogError($"Inner exception | Status Code: {innerResponse.StatusCode} | {innerResponse.Message}");
            _logger.LogError($"Stacktrace : {innerResponse.StackTrace}");
        }
    }
}

public static class ExceptionLoggingMiddlewareExtensions
{
    public static IApplicationBuilder UseExceptionLogging(this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<ExceptionLoggingMiddleware>();
    }
}