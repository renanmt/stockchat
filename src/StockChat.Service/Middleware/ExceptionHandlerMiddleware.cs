﻿using Microsoft.AspNetCore.Diagnostics;
using StockChat.Core.Exceptions;
using StockChat.Service.Models;
using System.Net;
using System.Text.Json;

namespace StockChat.Service.Middleware;

public class ExceptionHandlerMiddleware
{
    protected readonly RequestDelegate _next;
    private readonly IWebHostEnvironment _env;

    private readonly JsonSerializerOptions _serializerOptions =
        new() {PropertyNamingPolicy = JsonNamingPolicy.CamelCase};

    public ExceptionHandlerMiddleware(RequestDelegate next, IWebHostEnvironment env)
    {
        _next = next;
        _env = env;
    }

    public async Task Invoke(HttpContext context)
    {
        context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
        context.Response.ContentType = "application/json";

        var ex = context.Features.Get<IExceptionHandlerFeature>();
        if (ex != null)
        {
            var response = GetResponse(ex.Error);

            response.TraceIdentifier = context.TraceIdentifier;

            context.Response.StatusCode = response.StatusCode;

            ProcessError(response, ex.Error);

            await context.Response.WriteAsync(JsonSerializer.Serialize(response, _serializerOptions))
                .ConfigureAwait(false);
        }
    }

    protected virtual void ProcessError(ErrorResponse response, Exception exception)
    {
    }

    public ErrorResponse GetResponse(Exception ex)
    {
        var response = new ErrorResponse();

        var apiException = ex as ApiException;
        if (apiException != null)
        {
            response.FaultCode = (int) apiException.FaultCode;
            response.ExceptionTime = apiException.ExceptionTime;
            response.Message = apiException.Message;
            response.StackTrace = _env.IsDevelopment() ? apiException.StackTrace : null;
            response.StatusCode = (int) apiException.StatusCode;

            if (apiException.Data.Contains("additionalInfo"))
                response.AdditionalInfo = apiException.Data["additionalInfo"];

            if (apiException.InnerException != null)
                response.OriginalException =
                    $"{apiException.InnerException.Message}\r\n{apiException.InnerException.StackTrace}";
        }
        else
        {
            response.StatusCode = (int) HttpStatusCode.InternalServerError;
            response.Message = ex.Message;
            response.StackTrace = ex.StackTrace;
            response.ExceptionTime = DateTime.UtcNow;
        }

        return response;
    }
}

public static class ExceptionHandlerMiddlewareExtensions
{
    public static IApplicationBuilder UseExceptionHandler(this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<ExceptionHandlerMiddleware>();
    }
}