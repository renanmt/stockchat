﻿using System.Text.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using StackExchange.Redis;
using StockChat.Entities;

namespace StockChat.Service.Hubs;

public class ChatHub : Hub
{
    private const string Bot = "chat-update";
    
    private readonly UserConnections _userConnections;
    private readonly ISubscriber _subscriber;

    public ChatHub(UserConnections userConnections, ISubscriber subscriber)
    {
        _userConnections = userConnections;
        _subscriber = subscriber;
    }

    [Authorize]
    public async Task JoinRoom(Guid roomId)
    {
        var username = Context.User!.Identity!.Name!;

        _userConnections.AddOrUpdate(username,
            roomId, (key, oldValue) => roomId);

        await Groups.AddToGroupAsync(Context.ConnectionId, roomId.ToString());

        await Clients.Group(roomId.ToString())
            .SendAsync("ReceiveMessage", Bot, DateTime.UtcNow, $"{username} has joined the room.");

        await SendRoomUsers(roomId);
    }

    [Authorize]
    public async Task LeaveRoom()
    {
        var username = Context.User!.Identity!.Name!;

        if (_userConnections.TryGetValue(username, out var roomId))
        {
            _userConnections.TryRemove(username, out roomId);

            await Groups.RemoveFromGroupAsync(Context.ConnectionId, roomId.ToString());

            await Clients.Group(roomId.ToString())
                .SendAsync("ReceiveMessage", Bot, DateTime.UtcNow, $"{Context.User!.Identity!.Name} has left the room.");
            
            await SendRoomUsers(roomId);
        }
    }

    [Authorize]
    public async Task SendMessage(string message)
    {
        var username = Context.User!.Identity!.Name!;

        if (_userConnections.TryGetValue(username, out var roomId))
        {
            var messageTime = DateTime.UtcNow;
            
            if (message.StartsWith("/"))
            {
                var botMessage = new BotMessage()
                {
                    RoomId = roomId,
                    Body = message,
                    Username = username,
                    CreatedAt = messageTime
                };
                
                var botMessageJson = JsonSerializer.Serialize(botMessage);
                
                await _subscriber.PublishAsync("BotCommand", botMessageJson);
            }
            await Clients.Group(roomId.ToString()).SendAsync("ReceiveMessage", username, messageTime, message);
        }
    }

    public async Task BotResponse(string botResponseJson)
    {
        var botReponse = JsonSerializer.Deserialize<BotResponse>(botResponseJson);
        
        await Clients.Group(botReponse.RoomId.ToString())
            .SendAsync("ReceiveMessage", $"bot:{botReponse.BotName}", DateTime.UtcNow, botReponse.Message);
    }

    private async Task SendRoomUsers(Guid roomId)
    {
        var username = Context.User!.Identity!.Name!;

        var users =
            _userConnections
                .Where(c => c.Value == roomId)
                .Select(c => c.Key)
                .OrderBy(u => u);

        await Clients.Group(roomId.ToString())
            .SendAsync("ConnectedUsers", users);
    }
}