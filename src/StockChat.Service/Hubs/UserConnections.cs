using System.Collections.Concurrent;
using StockChat.Entities;

namespace StockChat.Service.Hubs;

public class UserConnections : ConcurrentDictionary<string, Guid>
{
}