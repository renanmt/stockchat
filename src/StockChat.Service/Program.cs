using Serilog;
using StockChat.Service.Bootstrap;
using StockChat.Service.Hubs;
using StockChat.Service.Middleware;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((context, configuration) =>
    configuration
        .MinimumLevel.Information()
        .WriteTo.Console());

SwaggerBootstrap.RegisterServices(builder.Services);
SecurityBootstrap.RegisterServices(builder.Services, builder.Configuration);
ServicesBootstrap.RegisterServices(builder.Services, builder.Configuration);

var app = builder.Build();

//
// Add custom exception handling to standardize response messages
//
app.UseExceptionHandler(options => options.UseExceptionLogging());

// We may choose to hide this when moved to production
app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();
app.MapHub<ChatHub>("/chat");

app.UseCors();

app.Run();