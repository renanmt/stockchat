﻿using System.Text.Json;
using FluentValidation;
using StackExchange.Redis;
using StockChat.Core.Business;
using StockChat.Core.Business.Validators;
using StockChat.Core.Models;
using StockChat.Core.Repository;
using StockChat.Core.Workflow;
using StockChat.Entities;
using StockChat.Repository.SqlServer;
using StockChat.Security;
using StockChat.Service.Hubs;
using StockChat.Service.Models;

namespace StockChat.Service.Bootstrap;

public static class ServicesBootstrap
{
    public static void RegisterServices(IServiceCollection services, IConfiguration configuration)
    {
        services.AddControllers().AddJsonOptions(config =>
            config.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        );

        var redisHost = configuration.GetConnectionString("Redis");
        services.AddSignalR().AddRedis(options =>
        {
            options.Configuration.EndPoints.Add(redisHost);
            options.Configuration.ClientName = "StockChat.Service";
        });

        services.AddSingleton<IConnectionMultiplexer>(ConnectionMultiplexer.Connect(redisHost));
        services.AddScoped<ISubscriber>(provider => provider.GetService<IConnectionMultiplexer>()!.GetSubscriber());

        services.AddAutoMapper(config =>
        {
            config.CreateMap<User, NewUserResponse>();
            config.CreateMap<User, GetUserResponse>();
        });

        services.AddTransient<IUserRepository, UserRepository>(implementationFactory =>
            new UserRepository(configuration.GetConnectionString("StockChat")));
        services.AddTransient<IUserBusiness, UserBusiness>();
        services.AddTransient<IValidator<NewUser>, NewUserValidator>();
        services.AddTransient<IUserWorkflow, UserWorkflow>();

        services.AddSingleton<UserConnections>();

        services.AddSingleton<IPasswordStorage, PasswordStorage>();
        services.AddSingleton<ITokenService, TokenService>();
        services.AddSingleton(configuration.GetSection("Security").Get<SecurityConfiguration>());
    }
}