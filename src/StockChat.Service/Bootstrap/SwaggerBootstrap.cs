using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;

namespace StockChat.Service.Bootstrap;

public class SwaggerBootstrap
{
    public static void RegisterServices(IServiceCollection services)
    {
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen(options =>
        {
            options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
            {
                Description = "Bearer scheme authentication (bearer {token})",
                In = ParameterLocation.Header,
                Name = "Authorization",
                Type = SecuritySchemeType.ApiKey
            });

            options.OperationFilter<SecurityRequirementsOperationFilter>();
        });
    }
}