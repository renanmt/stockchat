﻿namespace StockChat.Service.Models;

public class ErrorResponse
{
    public string? Message { get; set; }
    public int FaultCode { get; set; }
    public int StatusCode { get; set; }
    public string? StackTrace { get; set; }
    public DateTime ExceptionTime { get; set; }
    public string? TraceIdentifier { get; set; }
    public object? AdditionalInfo { get; set; }
    public object? OriginalException { get; set; }
}