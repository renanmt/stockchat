namespace StockChat.Service.Models;

public class GetUserResponse
{
    public Guid Id { get; set; }
    public string Email { get; set; } = null!;
    public string Username { get; set; } = null!;
}