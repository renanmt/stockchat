namespace StockChat.Service.Models;

public class NewUserResponse
{
    public Guid Id { get; set; }
    public string Username { get; set; } = null!;
}