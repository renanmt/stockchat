﻿namespace StockChat.Service.Models;

public class UserConnection
{
    public string Username { get; set; } = null!;
    public string Room { get; set; } = null!;
}