﻿using System.Security.Claims;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StockChat.Core.Models;
using StockChat.Core.Workflow;
using StockChat.Service.Models;

namespace StockChat.Service.Controllers;

[ApiController]
public class UserController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly IUserWorkflow _userWorkflow;

    public UserController(IUserWorkflow userWorkflow, IMapper mapper)
    {
        _userWorkflow = userWorkflow;
        _mapper = mapper;
    }

    [HttpPost("api/users")]
    public async Task<IActionResult> RegisterUser(NewUser newUser)
    {
        var user = await _userWorkflow.Register(newUser);
        var result = _mapper.Map<NewUserResponse>(user);

        return Created($"api/user/{user.Id}", result);
    }

    [HttpGet("api/users/{id}"), Authorize]
    public async Task<IActionResult> GetUser(Guid id)
    {
        var user = await _userWorkflow.GetUser(id);
        var result = _mapper.Map<GetUserResponse>(user);

        return Ok(result);
    }
    
    [HttpGet("api/users/me"), Authorize]
    public async Task<IActionResult> GetMyUser()
    {
        var id = new Guid(HttpContext.User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
        
        var user = await _userWorkflow.GetUser(id);
        var result = _mapper.Map<GetUserResponse>(user);

        return Ok(result);
    }

    [HttpPost("api/sessions")]
    public async Task<IActionResult> Login(UserCredentials credentials)
    {
        var result = await _userWorkflow.Login(credentials);

        return Ok(result);
    }
}