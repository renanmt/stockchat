using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StockChat.Entities;

namespace StockChat.Service.Controllers;

public class RoomsController : ControllerBase
{
    // Rooms mockup
    public static readonly IEnumerable<Room> Rooms = new List<Room>()
    {
        new Room()
        {
            Id = new Guid("F80D8ABE-78D4-40E1-A7C5-C0110A41BD97"),
            Name = "General"
        },
        new Room()
        {
            Id = new Guid("B1BBF609-C513-496C-BEAB-1AE85EC68348"),
            Name = "Stock Market Talk"
        }
    };
    
    [HttpGet("api/rooms"), Authorize]
    public IActionResult GetAvailableRooms()
    {
        //
        // Sending a fixed list for now, but this could be moved to the DB
        //
        var rooms = Rooms;

        return Ok(rooms);
    }
}