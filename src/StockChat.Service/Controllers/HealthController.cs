using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace StockChat.Service.Controllers;

public class HealthController : ControllerBase
{
    // GET
    [HttpGet("api/health")]
    public IActionResult Index()
    {
        return Ok("Hello world!");
    }
}