﻿using Microsoft.IdentityModel.Tokens;
using StockChat.Entities;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace StockChat.Security;

public class TokenService : ITokenService
{
    private readonly SecurityConfiguration _securityConfiguration;

    public TokenService(SecurityConfiguration securityConfiguration)
    {
        _securityConfiguration = securityConfiguration;
    }

    public string GenerateToken(User user)
    {
        var appSecret = _securityConfiguration.AppSecret;

        var key = Encoding.ASCII.GetBytes(appSecret);

        // generate token descriptor
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            // create claims
            Subject = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Name, user.Username)
            }, "serverAuth"),
            Expires = DateTime.UtcNow.AddDays(7),
            SigningCredentials =
                new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };

        // creating token
        var tokenHandler = new JwtSecurityTokenHandler();
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var jwt = tokenHandler.WriteToken(token);

        // return token
        return jwt;
    }
}