﻿using StockChat.Entities;

namespace StockChat.Security;

public interface ITokenService
{
    string GenerateToken(User user);
}