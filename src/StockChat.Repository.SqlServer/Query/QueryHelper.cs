using System.Collections.Concurrent;
using System.Reflection;

namespace StockChat.Repository.SqlServer.Query;

public static class QueryHelper
{
    private static readonly ConcurrentDictionary<string, string> Queries = new();

    public static string GetQuery(string fileName)
    {
        var resourceName = $"StockChat.Repository.SqlServer.Query.{fileName}";

        if (Queries.TryGetValue(fileName, out var query)) return query;

        var assembly = Assembly.GetExecutingAssembly();
        using (var stream = assembly.GetManifestResourceStream(resourceName))
        {
            if (stream != null)
            {
                using var reader = new StreamReader(stream);
                query = reader.ReadToEnd();
            }
        }

        Queries.AddOrUpdate(fileName, query!, (key, oldValue) => query!);

        return query!;
    }
}