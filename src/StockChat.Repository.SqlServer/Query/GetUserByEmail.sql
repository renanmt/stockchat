SELECT
      [Id],
      [Email],
      [Username],
      [PasswordHash]
FROM
      [sc].[User]
WHERE
      [Email] = @email
