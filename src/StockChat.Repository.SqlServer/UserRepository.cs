﻿using Dapper;
using Dapper.Apex;
using StockChat.Core.Repository;
using StockChat.Entities;
using StockChat.Repository.SqlServer.Query;

namespace StockChat.Repository.SqlServer;

public class UserRepository : SqlServerRepository, IUserRepository
{
    public UserRepository(string connectionString) : base(connectionString)
    {
    }

    public async Task CreateAsync(User user)
    {
        user.Id = Guid.NewGuid();
        await ExecuteAsync(connection => connection.InsertAsync(user));
    }

    public async Task<User?> GetAsync(Guid id)
    {
        return await ExecuteAndReturnAsync(connection => connection.GetAsync<User>(id));
    }

    public async Task<User?> GetByUsernameAsync(string username)
    {
        var result = await ExecuteAndReturnAsync(connection =>
            connection.QueryAsync<User>(QueryHelper.GetQuery("GetUserByUsername.sql"),
                new { username })
        );

        return result.FirstOrDefault();
    }

    public async Task<User?> GetByEmailAsync(string email)
    {
        var result = await ExecuteAndReturnAsync(connection =>
            connection.QueryAsync<User>(QueryHelper.GetQuery("GetUserByEmail.sql"),
                new { email })
        );

        return result.FirstOrDefault()!;
    }
}