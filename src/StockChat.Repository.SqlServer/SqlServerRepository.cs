﻿using System.Data;
using System.Data.SqlClient;

namespace StockChat.Repository.SqlServer;

public class SqlServerRepository
{
    public SqlServerRepository(string connectionString)
    {
        ConnectionString = connectionString;
    }

    public async Task ExecuteAsync(Func<IDbConnection, Task> action)
    {
        using (var connection = new SqlConnection(ConnectionString))
        {
            await connection.OpenAsync();
            await action(connection);
            await connection.CloseAsync();
        }
    }

    public async Task<T> ExecuteAndReturnAsync<T>(Func<IDbConnection, Task<T>> function)
    {
        using (var connection = new SqlConnection(ConnectionString))
        {
            await connection.OpenAsync();
            var result = await function(connection);
            await connection.CloseAsync();

            return result;
        }
    }

    protected string ConnectionString { get; }
}